const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const { secret_key } = require("../configs");

const verifyJwt = promisify(jwt.verify);
module.exports.authenticate = (req, res, next) => {
  const token = req.header("token");

  verifyJwt(token, secret_key)
    .then((decoded) => {
      if (decoded) {
        req.user = decoded;
        return next();
      }
    })
    .catch(() => res.status(401).json({ message: "User is not authentecated" }));
};
