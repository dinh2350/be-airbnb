const jwt = require("jsonwebtoken");

// userTypeArray = ["admin", "client"]
// 1. "admin" --> "admin" = user.userType ==> next()
// 2. "client" --> "client" = user.userType = Next()
module.exports.authorize = (userTypeArray) => {
  return (req, res, next) => {
    const { user } = req;

    if (userTypeArray.findIndex((elm) => elm === user.type) > -1) return next();
    // if (userType === user.userType) return next();
    return res.status(403).json({
      message: "Ban da dang nhap, nhung ko co quyen xem",
    });
  };
};
