const { Schema, model } = require("mongoose");

const locationSchema = new Schema({
  name: String,
  province: String,
  country: String,
  image: String,
  valueate: Number,
});

const Location = model("Location", locationSchema);

module.exports = {
  Location,
  locationSchema,
};
