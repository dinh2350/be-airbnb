const { Schema, model } = require("mongoose");

const ticketSchema = new Schema({
  checkIn: Date,
  checkOut: Date,
  userId: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  roomId: {
    type: Schema.Types.ObjectId,
    ref: "Room",
  },
});

const Ticket = model("Ticket", ticketSchema);

module.exports = {
  Ticket,
  ticketSchema,
};
