const { Schema, model } = require("mongoose");

const roomSchema = new Schema({
  name: String,
  guests: Number,
  bedRoom: Number,
  bath: Number,
  description: String,
  images: [String],
  price: Number,
  elevator: Boolean,
  hotTub: Boolean,
  pool: Boolean,
  indoorFireplace: Boolean,
  dryer: Boolean,
  gym: Boolean,
  kitchen: Boolean,
  wifi: Boolean,
  heating: Boolean,
  cableTV: Boolean,
  locationId: {
    type: Schema.Types.ObjectId,
    ref: "Location",
  },
});

const Room = model("Room", roomSchema);

module.exports = {
  Room,
  roomSchema,
};
