const { Schema, model } = require("mongoose");

const tokenSchema = new Schema({
  accessTokenByClass: String,
  className: String,
});

const Token = model("Token", tokenSchema);

module.exports = {
  Token,
  tokenSchema,
};
