const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  name: String,
  email: String,
  password: String,
  phone: String,
  birthday: Date,
  gender: Boolean,
  address: String,
  type: String,
  avatar: String,
  tickets: [
    {
      type: Schema.Types.ObjectId,
      ref: "Ticket",
    },
  ],
});

const User = model("User", userSchema);

module.exports = {
  User,
  userSchema,
};
