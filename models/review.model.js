const { Schema, model } = require("mongoose");

const reviewSchema = new Schema(
  {
    roomId: {
      type: Schema.Types.ObjectId,
      ref: "Room",
    },
    content: String,
    userId: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: { createdAt: "created_at" } }
);

const Review = model("Review", reviewSchema);

module.exports = {
  Review,
  reviewSchema,
};
