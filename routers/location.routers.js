const express = require("express");
const {
  create,
  getAll,
  getDetail,
  update,
  remove,
  uploadImage,
  getLocationByValueate,
} = require("../controllers/location.controller");
const { authenticate } = require("../middlewares/authenticate.middlewares");
const { authorize } = require("../middlewares/authorization.middlewares");
const { uploadImageSingle } = require("../middlewares/upload-image.middlewares");
const locationRouter = express.Router();

locationRouter.post("/", authenticate, authorize(["ADMIN"]), create);
locationRouter.post(
  "/upload-images/:id",
  authenticate,
  authorize(["ADMIN"]),
  uploadImageSingle("location"),
  uploadImage
);
locationRouter.get("/by-valueate", getLocationByValueate);
locationRouter.get("/", getAll);
locationRouter.get("/:id", getDetail);
locationRouter.put("/:id", authenticate, authorize(["ADMIN"]), update);
locationRouter.delete("/:id", authenticate, authorize(["ADMIN"]), remove);

module.exports = {
  locationRouter,
};
