const express = require("express");
const rootRouter = express.Router();
const { locationRouter } = require("../routers/location.routers");
const { userRouter } = require("../routers/user.routers");
const { authRouter } = require("./auth.routers");
const { roomRouter } = require("./room.routers");
const { reviewRouter } = require("./review.routers");
const { ticketRouter } = require("./ticket.routers");

rootRouter.use("/locations", locationRouter);
rootRouter.use("/users", userRouter);
rootRouter.use("/auth", authRouter);
rootRouter.use("/rooms", roomRouter);
rootRouter.use("/reviews", reviewRouter);
rootRouter.use("/tickets", ticketRouter);

module.exports = {
  rootRouter,
};
