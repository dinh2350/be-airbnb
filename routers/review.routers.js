const express = require("express");
const { create, getListByRoom, getDetail, update, remove } = require("../controllers/review.controller");
const reviewRouter = express.Router();
const { authenticate } = require("../middlewares/authenticate.middlewares");
const { authorize } = require("../middlewares/authorization.middlewares");

reviewRouter.post("/", authenticate, authorize(["ADMIN"]), create);
reviewRouter.get("/byRoom", getListByRoom);
reviewRouter.get("/:id", getDetail);
reviewRouter.put("/:id", authenticate, authorize(["ADMIN"]), update);
reviewRouter.delete("/:id", authenticate, authorize(["ADMIN"]), remove);

module.exports = {
  reviewRouter,
};
