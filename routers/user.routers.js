const express = require("express");
const {
  create,
  getAll,
  getDetail,
  update,
  remove,
  getAllPagination,
  uploadAvatar,
} = require("../controllers/user.controller");
const { uploadImageSingle } = require("../middlewares/upload-image.middlewares");
const userRouter = express.Router();
const { authenticate } = require("../middlewares/authenticate.middlewares");
const { checkExist } = require("../middlewares/validator/checkExist");
const { User } = require("../models/user.model");
const { authorize } = require("../middlewares/authorization.middlewares");
userRouter.post("/", authenticate, authorize(["ADMIN"]), create);
userRouter.get("/", getAll);
userRouter.get("/pagination", getAllPagination);
userRouter.get("/:id", getDetail);
userRouter.put("/:id", authenticate, authorize(["ADMIN"]), update);
userRouter.delete("/:id", authenticate, authorize(["ADMIN"]), checkExist(User), remove);
userRouter.post("/upload-avatar", authenticate, uploadImageSingle("avatar"), uploadAvatar);
module.exports = {
  userRouter,
};
