const express = require("express");
const { generateTokenKey, getAllTokenKey } = require("../controllers/auth.controller");
const tokenRouter = express.Router();

tokenRouter.post("/generate-token-key", generateTokenKey);
tokenRouter.get("/fetch-token-key", getAllTokenKey);
module.exports = {
  tokenRouter,
};
