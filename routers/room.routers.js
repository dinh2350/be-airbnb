const express = require("express");
const { create, getAll, getDetail, remove, update, uploadImage, booking } = require("../controllers/room.controller");
const { authenticate } = require("../middlewares/authenticate.middlewares");
const { authorize } = require("../middlewares/authorization.middlewares");
const { uploadImageMultiple } = require("../middlewares/upload-image.middlewares");
const roomRouter = express.Router();

roomRouter.post("/booking/:id", authenticate, booking);
roomRouter.post("/", authenticate, authorize(["ADMIN"]), create);

roomRouter.get("/", getAll);
roomRouter.get("/:id", getDetail);

roomRouter.put("/:id", authenticate, authorize(["ADMIN"]), update);
roomRouter.delete("/:id", authenticate, authorize(["ADMIN"]), remove);

roomRouter.post("/upload-image/:id", uploadImageMultiple("room", 12), uploadImage);

module.exports = {
  roomRouter,
};
