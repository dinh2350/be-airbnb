const express = require("express");
const { getAll, getDetail, remove, update, create } = require("../controllers/ticket.controller");
const { authenticate } = require("../middlewares/authenticate.middlewares");
const { authorize } = require("../middlewares/authorization.middlewares");
const ticketRouter = express.Router();

ticketRouter.post("/", authenticate, authorize(["ADMIN"]), create);
ticketRouter.get("/", getAll);
ticketRouter.get("/:id", getDetail);
ticketRouter.put("/:id", authenticate, authorize(["ADMIN"]), update);
ticketRouter.delete("/:id", authenticate, authorize(["ADMIN"]), remove);

module.exports = {
  ticketRouter,
};
