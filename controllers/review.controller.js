const { Review } = require("../models/review.model");

const create = async (req, res) => {
  const { user } = req;
  const { roomId } = req.query;
  try {
    const reviewNew = new Review({ ...req.body, roomId, userId: user._id });
    await reviewNew.save();
    res.status(200).send(reviewNew);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getListByRoom = async (req, res) => {
  const { roomId } = req.query;
  const { skip, limit } = req.query;
  console.log("roomId : ", roomId);
  try {
    const reviewList = await Review.find(
      {
        roomId,
      },
      null,
      {
        skip: +skip,
        limit: +limit,
      }
    ).populate({
      path: "userId",
      select: "",
    });
    res.status(200).send(reviewList);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getDetail = async (req, res) => {
  const { id } = req.params;
  try {
    const reviewDetail = await Review.findOne({ _id: id }).exec();
    res.status(200).send(reviewDetail);
  } catch (error) {
    res.status(500).send(error);
  }
};

const update = async (req, res) => {
  const { id } = req.params;
  const { roomId, content, userId } = req.body;
  try {
    let reviewEdit = await Review.findOne({ _id: id }).exec();
    if (reviewEdit) {
      reviewEdit.roomId = roomId;
      reviewEdit.content = content;
      reviewEdit.userId = userId;
      await reviewEdit.save();
      res.status(200).send(reviewEdit);
    } else {
      res.status(404).send(`not found`);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const remove = async (req, res) => {
  const { id } = req.params;
  try {
    const reviewDelete = await Review.deleteOne({ _id: id });
    res.status(200).send(reviewDelete);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  create,
  getListByRoom,
  getDetail,
  update,
  remove,
};
