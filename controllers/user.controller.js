const { User } = require("../models/user.model");
const bcrypt = require("bcryptjs");
const { host } = require("../configs");
const create = async (req, res) => {
  const { name, email, phone, password, birthday, gender, address, type } = req.body;
  try {
    // tạo ra một chuỗi ngẫu nhiên
    const salt = bcrypt.genSaltSync(10);
    // mã hóa salt + password
    const hashPassword = bcrypt.hashSync(password, salt);
    const userNew = new User({ name, email, password: hashPassword, phone, birthday, gender, address, type });
    await userNew.save();
    res.status(200).send(userNew);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getAll = async (req, res) => {
  try {
    const userList = await User.find();
    res.status(200).send(userList);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getDetail = async (req, res) => {
  const { id } = req.params;
  console.log("id : ", id);
  try {
    const userDetail = await User.findOne({ _id: id }).exec();
    if (userDetail) {
      res.status(200).send(userDetail);
    } else {
      res.status(404).send("Not Found");
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const update = async (req, res) => {
  const { id } = req.params;
  const { name, email, password, phone, birthday, gender, address, type } = req.body;
  try {
    let userEdit = await User.findOne({ _id: id }).exec();
    if (userEdit) {
      // name, email, password, phone, birthday, gender, address
      userEdit.name = name;
      userEdit.email = email;
      userEdit.password = password;
      userEdit.phone = phone;
      userEdit.birthday = birthday;
      userEdit.gender = gender;
      userEdit.address = address;
      userEdit.type = type;

      await userEdit.save();
      res.status(200).send(userEdit);
    } else {
      res.status(404).send(`not found`);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const remove = async (req, res) => {
  const { id } = req.params;
  try {
    const userDelete = await User.deleteOne({ _id: id });
    res.status(200).send(userDelete);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getAllPagination = async (req, res) => {
  const { skip, limit } = req.query;
  console.log(skip, limit);
  try {
    const userList = await User.find({}, null, {
      skip: +skip,
      limit: +limit,
    }).exec();
    res.status(200).send(userList);
  } catch (error) {
    res.status(500).send(error);
  }
};

const uploadAvatar = async (req, res) => {
  const { file } = req;
  const urlImage = `${host}/${file.path}`;
  const { user } = req;
  try {
    const userFound = await User.findOne({
      email: user.email,
    });
    userFound.avatar = urlImage;
    await userFound.save();
    res.status(200).send(userFound);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  create,
  getAll,
  getDetail,
  update,
  remove,
  getAllPagination,
  uploadAvatar,
};
