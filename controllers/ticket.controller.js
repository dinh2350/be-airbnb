const { Ticket } = require("../models/ticket.model");

const create = async (req, res) => {
  try {
    const { roomId, checkIn, checkOut, userId } = req.body;
    const newTicket = new Ticket({ roomId, checkIn, checkOut, userId });
    await newTicket.save();
    res.send(newTicket);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getAll = async (req, res) => {
  const { skip, limit } = req.query;
  try {
    const ticketList = await Ticket.find({}, null, {
      skip: +skip,
      limit: +limit,
    })
      .populate({
        path: "roomId",
        select: "-__v",
      })
      .populate({
        path: "userId",
        select: "-__v",
      });
    res.status(200).send(ticketList);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getDetail = async (req, res) => {
  const { id } = req.params;
  try {
    const ticketDetail = await Ticket.findOne({
      _id: id,
    }).exec();
    res.status(200).send(ticketDetail);
  } catch (error) {
    res.status(500).send(error);
  }
};

const update = async (req, res) => {
  const { id } = req.params;
  const { roomId, checkIn, checkOut, userId } = req.body;
  try {
    let ticketEdit = await Ticket.findOne({ _id: id }).exec();
    if (ticketEdit) {
      ticketEdit.roomId = roomId;
      ticketEdit.checkIn = checkIn;
      ticketEdit.checkOut = checkOut;
      ticketEdit.userId = userId;

      await ticketEdit.save();
      res.status(200).send(ticketEdit);
    } else {
      res.status(404).send(`not found`);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const remove = async (req, res) => {
  const { id } = req.params;
  try {
    const ticketDelete = await Ticket.deleteOne({ _id: id });
    res.status(200).send(ticketDelete);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  create,
  getAll,
  getDetail,
  update,
  remove,
};
