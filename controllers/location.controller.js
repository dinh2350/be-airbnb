const { host } = require("../configs");
const { Location } = require("../models/location.model");

const create = async (req, res) => {
  try {
    const locationNew = new Location({ ...req.body });
    await locationNew.save();
    res.status(200).send(locationNew);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getAll = async (req, res) => {
  const { location, skip, limit } = req.query;
  console.log(location, skip, limit);
  let reqQuery = {};
  let option = {};
  try {
    if (location) {
      reqQuery = {
        name: { $regex: ".*" + location + ".*", $options: "i" },
      };
    }
    if (skip) {
      option = {
        ...option,
        skip: +skip,
      };
    }
    if (limit) {
      option = {
        ...option,
        limit: +limit,
      };
    }

    const filterLocation = await Location.find(reqQuery, null, option);
    res.status(200).send(filterLocation);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getDetail = async (req, res) => {
  const { id } = req.params;
  try {
    const locationDetail = await Location.findOne({ _id: id }).exec();
    if (locationDetail) {
      res.status(200).send(locationDetail);
    } else {
      res.status(404).send("Not Found");
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const update = async (req, res) => {
  const { id } = req.params;
  const { name, country, province, image, valueate } = req.body;
  try {
    let locationEdit = await Location.findOne({ _id: id }).exec();
    if (locationEdit) {
      locationEdit.name = name;
      locationEdit.country = country;
      locationEdit.province = province;
      locationEdit.valueate = valueate;

      await locationEdit.save();
      res.status(200).send(locationEdit);
    } else {
      res.status(404).send(`not found`);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const remove = async (req, res) => {
  const { id } = req.params;
  try {
    const locationDelete = await Location.deleteOne({ _id: id });
    res.status(200).send(locationDelete);
  } catch (error) {
    res.status(500).send(error);
  }
};

const uploadImage = async (req, res) => {
  const { file } = req;
  const urlImage = `${host}/${file.path}`;
  const { id } = req.params;
  try {
    const locationFound = await Location.findOne({
      _id: id,
    });
    locationFound.image = urlImage;
    await locationFound.save();
    res.status(200).send(locationFound);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getLocationByValueate = async (req, res) => {
  const { valueate } = req.query;
  try {
    const locationList = await Location.find({
      valueate: { $gt: valueate },
    });
    res.status(200).send(locationList);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  create,
  getAll,
  getDetail,
  update,
  remove,
  uploadImage,
  getLocationByValueate,
};
