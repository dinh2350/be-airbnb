const { User } = require("../models/user.model");
const { Token } = require("../models/token.model");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { secret_key } = require("../configs");
const login = async (req, res) => {
  const { email, password } = req.body;
  // b1 : tìm ra user đang đăng nhập dựa trên trên email
  const user = await User.findOne({ email }).exec();

  if (user) {
    // b2 : kiểm mật khẩu có đúng hay không
    const isAuth = bcrypt.compareSync(password, user.password);
    if (isAuth) {
      const token = jwt.sign({ _id: user._id, email: user.email, type: user.type }, secret_key);
      res.status(200).send({ message: "Đăng Nhập Thành Công ! ", user, token });
    } else {
      res.status(500).send({ message: "Tài khoãng hoặc mật khẩu không đúng" });
    }
  } else {
    res.status(404).send({ message: "Không tìm thấy email phù hợp" });
  }
};

const register = async (req, res) => {
  const { name, email, phone, password, birthday, gender, address } = req.body;
  console.log(name, email, phone, password, birthday, gender, address);
  try {
    // tạo ra một chuỗi ngẫu nhiên
    const salt = bcrypt.genSaltSync(10);
    // mã hóa salt + password
    const hashPassword = bcrypt.hashSync(password, salt);
    const userNew = new User({ name, email, password: hashPassword, phone, birthday, gender, address, type: "CLIENT" });
    await userNew.save();
    res.status(200).send(userNew);
  } catch (error) {
    res.status(500).send(error);
  }
};

const generateTokenKey = async (req, res) => {
  const { className, date } = req.body;
  try {
    const token = jwt.sign({ className }, secret_key, { expiresIn: 60 * 60 * 24 * date });
    const newTokenByClass = new Token({ accessTokenByClass: token, className });
    await newTokenByClass.save();
    res.status(200).send({ token });
  } catch (error) {
    res.status(500).send({ message: "tạo token thất bại" });
  }
};

const getAllTokenKey = async (req, res) => {
  const { className } = req.query;
  let reqQuery = {};
  if (className) {
    reqQuery = {
      className: { $regex: ".*" + className + ".*", $options: "i" },
    };
  }
  console.log(reqQuery);
  try {
    const tokenList = await Token.find(reqQuery);
    res.status(200).send(tokenList);
  } catch (error) {
    res.status(500).send("lấy token thất bại");
  }
};

module.exports = {
  login,
  register,
  generateTokenKey,
  getAllTokenKey,
};
