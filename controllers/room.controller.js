const { Room } = require("../models/room.model");
const { Location } = require("../models/location.model");
const { User } = require("../models/user.model");
const { Ticket } = require("../models/ticket.model");
const { host } = require("../configs");

const create = async (req, res) => {
  try {
    const roomNew = new Room({ ...req.body });
    await roomNew.save();
    res.status(200).send(roomNew);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getAll = async (req, res) => {
  const { locationId, skip, limit } = req.query;
  console.log("locationId : ", locationId);
  let queryRoom;
  try {
    if (locationId) {
      queryRoom = {
        locationId,
      };
    } else {
      queryRoom = {};
    }
    roomList = await Room.find(queryRoom, null, {
      skip: +skip,
      limit: +limit,
    }).populate({
      path: "locationId",
      select: "-_id -__v",
    });
    res.status(200).send(roomList);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getDetail = async (req, res) => {
  const { id } = req.params;
  console.log("id : ", id);
  try {
    const roomDetail = await Room.findOne({ _id: id }).exec();
    res.status(200).send(roomDetail);
  } catch (error) {
    res.status(500).send(error);
  }
};

const update = async (req, res) => {
  const { id } = req.params;
  const {
    name,
    guests,
    bedRoom,
    bath,
    description,
    images,
    price,
    elevator,
    hotTub,
    pool,
    indoorFireplace,
    dryer,
    gym,
    kitchen,
    wifi,
    heating,
    cableTV,
    locationId,
  } = req.body;
  try {
    let roomEdit = await Room.findOne({ _id: id }).exec();
    if (roomEdit) {
      roomEdit.name = name;
      roomEdit.guests = guests;
      roomEdit.bedRoom = bedRoom;
      roomEdit.bath = bath;
      roomEdit.description = description;
      roomEdit.images = images;
      roomEdit.price = price;
      roomEdit.elevator = elevator;
      roomEdit.hotTub = hotTub;
      roomEdit.pool = pool;
      roomEdit.indoorFireplace = indoorFireplace;
      roomEdit.dryer = dryer;
      roomEdit.gym = gym;
      roomEdit.kitchen = kitchen;
      roomEdit.wifi = wifi;
      roomEdit.heating = heating;
      roomEdit.cableTV = cableTV;
      roomEdit.locationId = locationId;

      await roomEdit.save();
      res.status(200).send(roomEdit);
    } else {
      res.status(404).send(`not found`);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const remove = async (req, res) => {
  const { id } = req.params;
  try {
    const roomDelete = await Room.deleteOne({ _id: id });
    res.status(200).send(roomDelete);
  } catch (error) {
    res.status(500).send(error);
  }
};

const uploadImage = async (req, res) => {
  const { files } = req;
  console.log("files : ", files);
  const { id } = req.params;
  const listUrl = files.map((file) => {
    return `${host}/${file.path}`;
  });
  console.log("listUrl : ", listUrl);

  try {
    const roomDetail = await Room.findOne({
      _id: id,
    }).exec();
    roomDetail.images = listUrl;
    await roomDetail.save();
    res.status(200).send(roomDetail);
  } catch (error) {
    res.status(500).send(error);
  }
};

const booking = async (req, res) => {
  const { user } = req;
  const { roomId, checkIn, checkOut } = req.body;
  try {
    const newTicket = new Ticket({ checkIn, checkOut, userId: user._id, roomId });
    await newTicket.save();
    const userDetail = await User.findOne({ _id: user._id }).exec();
    userDetail.tickets.push(newTicket._id);
    await userDetail.save();
    res.status(200).send({
      message: "đặt phòng thành công",
      userDetail,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  create,
  getAll,
  getDetail,
  update,
  remove,
  uploadImage,
  booking,
};
