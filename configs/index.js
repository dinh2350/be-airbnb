require("dotenv").config();

let mongo_uri;
let secret_key;
let port;
let email;
let password;
let host;

switch (process.env.NODE_ENV) {
  case "local":
    mongo_uri = process.env.MONGO_LOCAL_URL;
    secret_key = process.env.SECRET_KEY_LOCAL;
    port = process.env.PORT_LOCAL;
    email = process.env.EMAIL_LOCAL;
    password = process.env.PASSWORD_LOCAL;
    host = `localhost:${port}`;
    break;

  case "production":
    mongo_uri =
      "mongodb+srv://hao9x0159:11M@chthailai11@airbnbv1cluster.cr7hx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    secret_key = "hao9x0159";
    email = "hao9x0159@gmail.com";
    password = "123456";
    // port: Heroku se tao cho minh
    host: "https://project-airbnb-0159.herokuapp.com";
    break;

  default:
    break;
}

module.exports = {
  mongo_uri,
  secret_key,
  port,
  email,
  password,
  host,
};
