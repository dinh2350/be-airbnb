// setup express
const express = require("express");
const { rootRouter } = require("./routers/root.routers");
const { tokenRouter } = require("./routers/admin.routers");
const { verifyTokenByClass } = require("./middlewares/token/verifyTokenByClass");

const { mongo_uri, port } = require("./configs");
const path = require("path");
const app = express();
app.use(express.json());
// cài static file
const publicPathDirectory = path.join(__dirname, "./public");
app.use("/public", express.static(publicPathDirectory));

app.use("/api", verifyTokenByClass, rootRouter);
app.use("/admin", tokenRouter);
const myport = process.env.PORT || port;
app.listen(myport, () => {
  console.log(`App listening on port ${myport}!`);
});

// setup db
const mongoose = require("mongoose");
mongoose.connect(mongo_uri, { useNewUrlParser: true, useUnifiedTopology: true });
